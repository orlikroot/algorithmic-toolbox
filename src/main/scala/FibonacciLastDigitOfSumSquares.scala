//Test OK Personal
object FibonacciLastDigitOfSumSquares extends App {

  def fib(n: Long): Long = {
    def fib_tail(n: Long, a: Long, b: Long): Long = n match {
      case 0 => a
      case _ => fib_tail(n - 1, b, (a + b) % 1000000)
    }

    fib_tail(n % 1500000, 0, 1)
  }

  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toLong
  println(((fib(n) % 10) * (fib(n+1) % 10)) %10)

}