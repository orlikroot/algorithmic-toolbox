object MaximumPairwise extends App{
  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toInt
  val line = scanner.nextLine()
  val data = (line.split(" ").map( x => BigInt(x)) take n).toList
  val a = data.max
  val tail = data.filterNot(x => x == a)

  val b = {
    if (n - tail.length > 1) {
      a
    } else {
      tail.max
    }
  }
  println(a*b)
}
