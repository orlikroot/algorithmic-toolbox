import java.util
import java.util.Collections

//Test OK. Personal
object BinarySearch extends App{

//  def binarySearch(arr: Array[Int], value: Int)(low: Int = 0, high: Int = arr.length - 1): Int = {
//
//    if (low > high)
//      return -1
//    val pivotElement = low + (high - low) / 2
//
//    if (arr(pivotElement) == value)
//      pivotElement
//
//    else if (arr(pivotElement) > value)
//      binarySearch(arr,
//        value)(low, pivotElement - 1)
//
//    else
//      binarySearch(arr,
//        value)(pivotElement + 1, high)
//  }

  val scanner = new java.util.Scanner(System.in)

  val n = scanner.nextLine().toInt

  val lineA = scanner.nextLine()
  val listA = (lineA.split(" ").map(x => x.toInt) take n).toList

  val m = scanner.nextLine().toInt
  val lineB = scanner.nextLine()
  val listB = (lineB.split(" ").map(x => x.toInt) take m).toList



//  println(listB.map(value => binarySearch(listA.toArray, value)()).mkString(" "))
  println(listB.map(value => util.Arrays.binarySearch(listA.toArray, value)).map(v => if(v < 0) {-1} else v).mkString(" "))
}
