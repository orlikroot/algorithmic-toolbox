//Test OK. Personal
object FractionalKnapsack extends App{

  val scanner = new java.util.Scanner(System.in)

  val line = scanner.nextLine()
  val result = (line.split(" ").map(x => x.toInt) take 2).toList
  val itemSize = result.head
  val backplateWeight = result.last
  val items = (0 until  itemSize).map(_ => {
      val line = scanner.nextLine()
      val result = (line.split(" ").map(x => x.toInt) take 2).toList
      val cost = result.head
      val weight = result.last
      Item(cost, weight, cost.toDouble/weight)
    }
  ).toList.sortBy(i => i.ration).reverse


  def stole(backplateWeight: Int, items: List[Item]): Double = {
      val item = items.head
      val res = if(backplateWeight <= item.weight) {
        item.cost * (backplateWeight.toDouble/item.weight)
      } else {
        item.cost +
          (if(items.tail.isEmpty) {0} else
          {stole(backplateWeight - item.weight, items.tail)})
      }
    BigDecimal(res).setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble
  }
  println(stole(backplateWeight, items))

}

case class Item(cost: Int, weight: Int, ration: Double)
