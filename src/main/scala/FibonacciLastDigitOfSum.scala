object FibonacciLastDigitOfSum extends App {
  def fibSum(n: Long): Long = {
    var first: Long = 0
    var second: Long = 1
    var count: Long = 0
    var sumF: Long = 0

    while (count < n) {
      val sum = first + second
      first = second
      second = sum
      count = count + 1
      sumF += first
    }

    sumF
  }

  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toLongOption.get
  println(fibSum(n) % 10)

}
