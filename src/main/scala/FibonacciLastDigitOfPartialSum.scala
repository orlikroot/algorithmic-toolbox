object FibonacciLastDigitOfPartialSum extends App {
  def fibSum(m: Long, n: Long): BigInt = {
    var first: BigInt = 0
    var second: BigInt = 1
    var count: Long = 0
    var sumF: BigInt = 0

    while (count < n) {
      val sum = first + second
      first = second
      second = sum

      count = count + 1
      if (count >= m) {
        sumF = sumF + first
      }

    }

    sumF
  }

  val scanner = new java.util.Scanner(System.in)

  val line = scanner.nextLine()
  val result = (line.split(" ").map(x => x.toInt) take 2).toList
  val m = result.head
  val n = result.last
  println(fibSum(m,n) % 10)

}
