//Test OK
object Fibonacci extends App {
  def fib(n: Long): Long = {
    var first = 0
    var second = 1
    var count = 0

    while(count < n){
      val sum = first + second
      first = second
      second = sum
      count = count + 1
    }
    first
  }

  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toInt
  println(fib(n))
}
