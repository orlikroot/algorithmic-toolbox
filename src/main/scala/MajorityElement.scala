
//Test OK. Personal!!!!
object MajorityElement extends App{

  def maj(data: List[Int]): Int = {

    val sorted = data.sorted
    val n = data.size/2
    val el = sorted(n)
    if (sorted.count(x => x == el) > n) 1 else 0

  }

  val scanner = new java.util.Scanner(System.in)

  val n = scanner.nextLine().toInt

  val lineA = scanner.nextLine()
  val listA = (lineA.split(" ").map(x => x.toInt) take n).toList

  println(maj(listA))
}
