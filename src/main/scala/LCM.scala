//Test OK
object LCM extends App {

  def gcd(a: Long, b: Long):Long=if (b==0) a.abs else gcd(b, a%b)

  def lcm(a: Long, b: Long)=(a*b).abs/gcd(a,b)

  val scanner = new java.util.Scanner(System.in)

  val line = scanner.nextLine()
  val result = (line.split(" ").map(x => x.toInt.toLong) take 2).toList
  val m = result.head
  val n = result.last
  println(lcm(m, n))

}
