//Test OK
object FibonacciHuge extends App{

  def pisano(m: Long): Long = {
    var prev: Long  = 0
    var curr: Long  = 1
    var res: Long  = 0

    (0.toLong until m*m).foreach(i => {
      {
        var temp: Long = 0
        temp = curr
        curr = (prev + curr) % m
        prev = temp

        if (prev == 0 && curr == 1)
          res = i + 1
      }
    })
    res
  }

  def fibonacciModulo(n: Long, m: Long): Long = {

    val pisanoPeriod = pisano(m)

    val np = n % pisanoPeriod

    var prev: Long = 0
    var curr: Long = 1

    if (n == 0) 0
    else if (n == 1) 1
    else {
      (1.toLong until np).foreach(_ => {
        var temp: Long = 0
        temp = curr
        curr = (prev + curr) % m
        prev = temp
      })
      curr % m
    }
  }

  val scanner = new java.util.Scanner(System.in)
  val line = scanner.nextLine()

  val result = (line.split(" ").map(x => x.toLong) take 2).toList
  val n = result.head
  val m = result.last
  println(fibonacciModulo(n, m))

}
