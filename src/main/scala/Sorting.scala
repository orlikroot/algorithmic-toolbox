//Test OK. Personal. Not improved

object Sorting extends App{

  def sortQ(arr: List[Int]) = {
    arr.sorted
  }

  val scanner = new java.util.Scanner(System.in)

  val n = scanner.nextLine().toInt

  val lineA = scanner.nextLine()
  val listA = (lineA.split(" ").map(x => x.toInt) take n).toList

  println(sortQ(listA).mkString(" "))

}
