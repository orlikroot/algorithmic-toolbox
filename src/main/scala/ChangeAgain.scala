//Test OK. Personal
object ChangeAgain extends App{

  val coins = List(4, 3, 1)

  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toInt

  def calc(coins: List[Int], money: Int): Int = {
    val requiredCoins = coins.iterator.filter(x => x <= n).toList
    val coin = requiredCoins.head
    val m = money % coin
    if (m == 0) {money/coin}
    else {
      money/coin + calc(coins.tail, m)
    }
  }
  println(calc(coins, n))

}
