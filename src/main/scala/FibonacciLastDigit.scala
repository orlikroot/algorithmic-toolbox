//Test OK
object FibonacciLastDigit extends App{
//  def fib(n: Long): BigInt = {
//    var first: BigInt = 0
//    var second: BigInt = 1
//    var count: Long = 0
//
//    while (count < n) {
//      val sum = first + second
//      first = second
//      second = sum
//      count = count + 1
//    }
//
//    first
//  }

  def fib(n: Int): Int = {
    def fib_tail(n: Int, a: Int, b: Int): Int = n match {
      case 0 => a
      case _ => fib_tail(n - 1, b, (a + b) % 1000000)
    }

    fib_tail(n % 1500000, 0, 1)
  }

  val scanner = new java.util.Scanner(System.in)
  val n = scanner.nextLine().toInt
  println(fib(n) % 10 )

}
