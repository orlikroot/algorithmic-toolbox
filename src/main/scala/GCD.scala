//Test OK
object GCD extends App{

  def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }

  val scanner = new java.util.Scanner(System.in)

  val line = scanner.nextLine()
  val result = (line.split(" ").map(x => x.toInt) take 2).toList
  val m = result.head
  val n = result.last
  println(gcd(m, n))

}
