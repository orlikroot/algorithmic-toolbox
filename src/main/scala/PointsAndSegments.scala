import java.util

object PointsAndSegments extends App{

//  def in(segment: Segment, point: Int) = {
//    segment.start <= point && segment.end >= point
//  }


//  def compareIn(segments: List[Segment], points: List[Int]): List[Int] = {
//    points.map(p => segments.count(s => in(s, p)))
//  }



//  def in(list: List[Int], v: Int) = {
//    if (util.Arrays.binarySearch(list.toArray, v) < 0) false else true
//  }

  def compareIn(segments: List[(Int, Int)], points: List[Int]): List[Int] = {
    points.map(p => segments.count(s => p >= s._1 && p<= s._2))

  }

  val scanner = new java.util.Scanner(System.in)
  val x = scanner.nextLine().split(" ").map(x => x.toInt) take 2

  val n = x.apply(0)
  val m = x.apply(1)

//  val segments: List[Segment] = (0 until  n).map(_ => {
//    val line = scanner.nextLine()
//    val result = (line.split(" ").map(x => x.toInt) take 2).toList
//    val start = result.head
//    val end = result.last
//    Segment(start, end)
//  }).toList

  val segments: List[(Int, Int)] = (0 until n).map(_ => {
    val line = scanner.nextLine()
    val result = (line.split(" ").map(x => x.toInt) take 2).toList
    val start = result.apply(0)
    val end = result.apply(1)
    (start -> end)
  }).toList

  val line = scanner.nextLine()
  val points = (line.split(" ").map(x => x.toInt) take m).toList

  println(compareIn(segments, points).mkString(" "))
}

//case class Segment(start: Int, end: Int)
