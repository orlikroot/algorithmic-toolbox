//Test OK. Personal
object CarFueling extends App{

  val scanner = new java.util.Scanner(System.in)

  val destination = scanner.nextLine().toInt
  val tank = scanner.nextLine().toInt
  val n = scanner.nextLine().toInt

  val line = scanner.nextLine()
  val stops: List[Int] = (((line.split(" ").map(x => x.toInt) take n).toList) ++ List(destination, 0)).sorted

  def getStopsCount(stopCount: Int, stops: List[Int]): Int = {
    if(stops.head == destination) {stopCount - 1}
    else {
      val start = stops.head
      val maxPath = start + tank
      val availableStops = stops.filter(s => s <= maxPath)
      val nextStop = availableStops.max
      val nextAvailableStops = List(nextStop) ++ stops.filterNot(s => s <= nextStop)
      getStopsCount(stopCount + 1, nextAvailableStops)
    }
  }

  val longDistance = stops.zip(stops.tail).map(x => x._2 - x._1).count(s => s > tank)
  val result = if(longDistance > 0) {-1}
  else if (destination < tank) {0}
  else {getStopsCount(0, stops)}

  println(result)
}
