//Test OK. Personal
object DotProduct extends App{

  def maxDotProduct(a: List[Int], b: List[Int]): Long = {
    val sortedA = a.sorted.reverse
    val sortedB = b.sorted.reverse
    sortedA.zip(sortedB).map(x => (x._1.toLong) * (x._2.toLong)).sum
  }


  val scanner = new java.util.Scanner(System.in)

  val n = scanner.nextLine().toInt

  val lineA = scanner.nextLine()
  val listA = (lineA.split(" ").map(x => x.toInt) take n).toList

  val lineB = scanner.nextLine()
  val listB = (lineB.split(" ").map(x => x.toInt) take n).toList

  println(maxDotProduct(listA, listB))

}
