#include <iostream>
#include <vector>

using std::vector;

vector<int> optimal_summands(int n) {
  vector<int> summands;
      for (int x = n, y = 1; x > 0; y++) {
          if (x <= 2 * y) {
              summands.push_back(x);
              x -= x;
          } else {
              summands.push_back(y);
              x -= y;
          }
      }
      return summands;
}

int main() {
  int n;
  std::cin >> n;
  vector<int> summands = optimal_summands(n);
  std::cout << summands.size() << '\n';
  for (size_t i = 0; i < summands.size(); ++i) {
    std::cout << summands[i] << ' ';
  }
}
