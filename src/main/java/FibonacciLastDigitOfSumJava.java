import java.util.Scanner;

class FibonacciLastDigitOfSumJava {
    static int fib(long n) {

        int f0 = 0;
        int f1 = 1;

        if (n == 0)
            return 0;
        if (n == 1)
            return 1;
        else {

            int rem = (int) (n % 60);

            if (rem == 0)
                return 0;

            for (int i = 2; i < rem + 3; i++) {
                int f = (f0 + f1) % 60;
                f0 = f1;
                f1 = f;
            }

            int s = f1 - 1;
            return s;
        }
    }

    public static void main(String args[]) {
        int m = 0;
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int result = (int) Math.abs(fib(n) -
                fib(m - 1));

        System.out.println(result % 10);
    }
}
