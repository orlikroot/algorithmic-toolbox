import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class LargestNumberJava {
    private static String largestNumber(Integer[] input) {
        StringBuilder result = new StringBuilder();

        Arrays.sort(input, (A, B) -> {
            String AB = String.valueOf(A) + String.valueOf(B) + "";
            String BA = String.valueOf(B) + String.valueOf(A) + "";
            return AB.compareTo(BA) >= 0 ? -1 : 1;
        });

        for (int i = 0; i < input.length ; i++) {
            result.append(input[i]);
        }

        return result.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Integer[] a = new Integer[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println(largestNumber(a));
    }
}

