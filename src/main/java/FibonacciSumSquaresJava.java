import java.util.Scanner;

public class FibonacciSumSquaresJava {

//    static int calculateSquareSumLastDigit(int n)
//    {
//        if (n <= 0)
//            return 0;
//
//        int fibo[] = new int[n+1];
//        fibo[0] = 0 ;
//        fibo[1] = 1 ;
//
//        long sum = (fibo[0] * fibo[0]) + (fibo[1] * fibo[1]);
//
//        for (int i = 2; i <= n; i++) {
//            fibo[i] = fibo[i - 1] + fibo[i - 2];
//            sum += (fibo[i] * fibo[i]);
//        }
//
//        return (int) sum % 10;
//    }

    public static long pisano(long m) {
        long prev = 0;
        long curr = 1;
        long res = 0;

        for (int i = 0; i < m * m; i++) {
            long temp = 0;
            temp = curr;
            curr = (prev + curr) % m;
            prev = temp;

            if (prev == 0 && curr == 1)
                res = i + 1;
        }
        return res;
    }


    public static long fibonacciModulo(long n,
                                       long m) {

        long pisanoPeriod = pisano(m);

        n = n % pisanoPeriod;

        long prev = 0;
        long curr = 1;

        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;

        for (int i = 0; i < n - 1; i++) {
            long temp = 0;
            temp = curr;
            curr = (prev + curr) % m;
            prev = temp;
        }
        return curr % m;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        long s = (fibonacciModulo(n, 1) * fibonacciModulo(n+1, 1)) % 10;
        System.out.println(s);
    }
}

