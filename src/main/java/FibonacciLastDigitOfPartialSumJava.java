import java.util.Scanner;

class FibonacciLastDigitOfPartialSumJava {
//    static long fib(long n) {
//
//        long f0 = 0;
//        long f1 = 1;
//
//        if (n == 0)
//            return 0;
//        if (n == 1)
//            return 1;
//        else {
//
//            int rem = (int) (n % 60);
//
//            if (rem == 0)
//                return 0;
//
//            for (int i = 2; i < rem + 3; i++) {
//                long f = (f0 + f1) % 60;
//                f0 = f1;
//                f1 = f;
//            }
//
//            long s = f1 - 1;
//            return s;
//        }
//    }
//
//    public static void main(String args[]) {
//        Scanner scanner = new Scanner(System.in);
//        long m = scanner.nextLong();
//        long n = scanner.nextLong();
//        int result = (int) Math.abs(fib(n) -
//                fib(m - 1));
//
//        System.out.println(result % 10);
//    }
        static long fib(long n)
        {

            long f0 = 0;
            long f1 = 1;

            if (n == 0)
                return 0;
            if (n == 1)
                return 1;
            else
            {

                long rem = (int) (n % 60);

                if(rem == 0)
                    return 0;

                for(long i = 2; i < rem + 3; i++)
                {
                    long f = (f0 + f1) % 60;
                    f0 = f1;
                    f1 = f;
                }

                long s = f1 - 1;
                return s;
            }
        }

        // Driver Code
        public static void main(String args[])
        {
            Scanner scanner = new Scanner(System.in);
            long from = scanner.nextLong();
            long to = scanner.nextLong();
            long result = Math.abs(fib(from) - fib(to - 1));

            System.out.println(result % 10);
        }
    }

