import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DotProductJava {
    static void maxDotProduct(int[][] points, int n) {
        Arrays.sort(points, (a, b) -> a[1] - b[1]);

        ArrayList<Integer> coords = new ArrayList<>();
        int i = 0;

        while (i < n) {
            int seg = points[i][1];
            coords.add(seg);
            int p = i + 1;

            if (p >= n)
                break;

            int cross = points[p][0];
            while (seg >= cross) {
                p += 1;

                if (p >= n)
                    break;

                cross = points[p][0];
            }
            i = p;
        }

        System.out.println(coords.size());
        for (Integer point : coords)
            System.out.print(point + " ");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }

        int[][] points = new int[n][2];
        for (int i = 0; i < n; i++) {
            points[i][0] = a[i];
            points[i][1] = b[i];
        }

        maxDotProduct(points, n);
    }
}

